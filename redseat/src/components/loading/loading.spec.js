import { shallowMount } from '@vue/test-utils';
import Loading from './loading.vue';

describe('Loading.vue', () => {
  it('mounts component and check if component exists', () => {
    // Mount component and check if component exists
    const wrapper = shallowMount(Loading);

    expect(wrapper.exists()).toEqual(true);
  });
});
