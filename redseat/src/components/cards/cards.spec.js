import { shallowMount } from '@vue/test-utils';
import Cards from './cards.vue';

describe('Cards.vue', () => {
  it('mounts component and check if component exists', () => {
    // Mount component and check if component exists
    const wrapper = shallowMount(Cards);

    expect(wrapper.exists()).toEqual(true);
  });
});
