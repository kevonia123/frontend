import { Component, Vue } from 'vue-property-decorator';
import { Role } from '@/enums/role';
import { Roles } from '@/entities/role/role';
import { Register } from '@/services/auth-register';
import { UserServices } from '@/services/user-services';
import { UserRegistration } from '@/entities/user-registration/user-registration';
import { app } from '@/store'
import env from '@/config/env';
import Loading from '@/components/loading';
import { ValidationObserver, ValidationProvider } from "vee-validate";

@Component({
  components: {
    Loading,
    ValidationObserver,
    ValidationProvider
  },
  name: 'register',
})
class RegisterPage extends Vue {

  private fname: string = 'Kevonia';
  private lname: string = 'Tomlinson';
  private password: string = 'testpassword';
  private passwordConfirmation: string = 'testpassword';
  private email: string = 'kevonia1234@gmail.com';
  private phone: string = '12345678';
  private zipCode: string = '1234567';
  private addressLine1: string = 'Testaddress';
  private addressLine2: string = 'Testaddress';
  private role!: Role[];
  private readonly logo = env.brand.logo;
  private isFullPage = true;
  private error = false;
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------

  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------

  public get loading() {
    return app.Loading;
  }

  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------
  public async signup() {

    let userrole: String[] = ['user'];
    app.ToolgeLoading(true);

    const signupdata: UserRegistration = {
      name: this.fname + " " + this.lname,
      email: this.email,
      password: this.password,
      phone: this.phone,
      passwordConfirmation: this.passwordConfirmation,
      zipCode: this.zipCode,
      addressLine1: this.addressLine1,
      addressLine2: this.addressLine2,
      role: userrole,
    }

    try {
      const exist = await UserServices.existsByEmail(signupdata.email);
      if (exist) {
        app.ToolgeLoading(false);
        this.error = exist;
        this.$buefy.toast.open({
          duration: 5000,
          message: `Email is already in use`,
          position: 'is-top',
          type: 'is-danger'
        })
      } else {
        const register = await Register.Signup(signupdata)

        if (register != undefined) {
          app.ToolgeLoading(false);

          this.$buefy.toast.open({
            duration: 5000,
            message: `registration successful`,
            position: 'is-top',
            type: 'is-success'
          })
        } else {
          app.ToolgeLoading(false);
        }
      }

    } catch (error) {
      app.ToolgeLoading(false);

      this.$buefy.toast.open({
        duration: 5000,
        message: `Something went wrong please contact RedSeat support`,
        position: 'is-top',
        type: 'is-danger'
      })
    }




  }
  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Lifecycle Hooks
  // --------------------------------------------------------------------------

  private mounted() {
    // TODO: stuff to do when this component loads.
    app.ToolgeLoading(false);
  }
}

export {
  RegisterPage as default,
  RegisterPage,
};
