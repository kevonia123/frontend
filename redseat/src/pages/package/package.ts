import { Component, Vue } from 'vue-property-decorator';
import { packageInvoice, invoicerequest, userpackage } from '@/store'
import { PackageInvoice } from '@/entities/package-invoice/package-invoice';
import { Fees } from '@/entities/fees/fees';

@Component({
  components: {},
  name: 'package',
})
class Package extends Vue {
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------
  public thispackage: PackageInvoice = {};

  public file = null;

  public isupload = false;


  private data: Fees[] = []

  private columns = [
    {
      field: 'name',
      label: 'DESCRIPTION',
    },
    {
      field: 'value',
      label: 'AMOUNT',
    }
  ]

  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------


  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

  private async showUpload() {
    this.isupload = true;

  }

  private async upload() {

    this.getBase64(this.file).then(
      async data => {

        const filetype = data.split(';')

        const base64 = filetype[1].split(',')

        const filedata = {
          name: this.file.name,
          base64: base64[1],
          type: filetype[0],
          username: "Kevonia",
          packageid: 2
        }

        const invoice = await invoicerequest.fetchInvoice(filedata)

        this.thispackage = await packageInvoice.fetchPackageInvoice(Number(this.$route.params.id));

        this.isupload = false;
        this.file = null;

        if (invoice != undefined) {
          this.$buefy.toast.open({
            message: 'Saved',
            type: 'is-success'
          })
        }

      }
    );

  }




  private showDocument(base64URL: string) {
    var win = window.open();
    win.document.write('<iframe src="' + base64URL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>');

  }

  private removeInvoice(id: number) {
    const invoices = this.thispackage.invoices.filter(item => item.id !== id);

    invoicerequest.removeInvoice(id);

    this.thispackage.invoices = invoices;

    this.$buefy.toast.open({
      message: 'Removed',
      type: 'is-success'
    });

  }


  private getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  // --------------------------------------------------------------------------
  // [Private] Lifecycle Hooks
  // --------------------------------------------------------------------------
  private async beforeMount() {

    const id = Number(this.$route.params.id)

    const found = userpackage.Userpackage.packlist.find(data => data.id == id)

    if (found == undefined) {
      this.$router.push('/404')

    } else {

      this.thispackage = await packageInvoice.fetchPackageInvoice(id);

      this.data = this.thispackage.billing.fee;

      let total: number[] = [];
      let key = 0;

      this.data.forEach((value, key) => {
        total.push(Number(value.value))
        key = key + 1;
      });


      this.data.push({ "id": key + 0, "name": "Balance Due", "value": total.reduce((a, b) => Number(a) + Number(b), 0) });
    }

  }
  private async mounted() {
    // TODO: stuff to do when this component loads.


  }
}

export {
  Package as default,
  Package,
};
