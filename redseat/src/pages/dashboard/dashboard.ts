import { Component, Vue } from 'vue-property-decorator';


import cards from '@/components/cards'

import { userpackage, user } from '@/store'
import { Package } from '@/entities/package/package';

import { PackageLocation } from '@/enums/packagelocation';

import { PackagesStatus } from '@/enums/packagesstatus';

@Component({
  components: { cards },
  name: 'dashboard',
})
class Dashboard extends Vue {

  private packages: Package[] = [];
  private balance: number = 0;
  private transit: number = 0;
  private points: number = 600;
  private ready: number = 0;
  private fname: string = "";
  private lname: string = "";
  private id!: number;


  private columns = [
    {
      field: 'description',
      label: 'DESCRIPTION',
    },
    {
      field: 'trackingNumber',
      label: 'TRACKING NUMBER',
    },
    {
      field: 'status',
      label: 'STATUS',
    }
  ]
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------

  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------
  public async navigate(item) {
    this.$router.push({ path: '/package/' + item.id })
  }
  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Lifecycle Hooks
  // --------------------------------------------------------------------------
  private async beforeMount() {
    this.id = user.User?.id;
    const userpackages = await userpackage.fetchUserPackage(this.id);
    const name = userpackages.name.split(' ')
    this.fname = name[0];
    this.lname = name[1];
    userpackages.packlist.forEach((data, index) => {
      if (data.location == PackageLocation.WAREHOUSE && data.Status != PackagesStatus.COLLECTED) {
        this.balance = this.balance + data.value;
        this.ready = index;
      }
      if (data.Status != PackagesStatus.INTRANSIT) {
        this.transit = index;
      }
      this.packages.push(data);
    });
  }

  private async mounted() {
    // TODO: stuff to do when this component loads
  }

}

export {
  Dashboard as default,
  Dashboard,
};
