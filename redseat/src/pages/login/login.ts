import { Component, Vue } from 'vue-property-decorator';
import { user, app } from '@/store'
import { Loginrequest } from '@/entities/loginrequest/loginrequest';
import  Loading from '@/components/loading'
import env from '@/config/env';

@Component({
  components: {Loading},
  name: 'login',
})
class Login extends Vue {


  private username = 'granville.crona@example.com';
  private password = 'testpassword';
  private readonly logo = env.brand.logo;
 
  private isFullPage= true;
  private error= false;


  constructor() {
    super();
  }


  public get loading(){
    return app.Loading;
  }

  public async login() {

    const logindata = {
      email: this.username,
      password: this.password
    }

    try {

      this.error = false;
      app.ToolgeLoading(true);

      const auth = await user.Login(logindata);

      sessionStorage.setItem('token', auth.accessToken);

      const CurrentUser = await user.getUser(auth.id)

      if (auth != undefined) {
        this.$router.push('/')

      }

    } catch (error) {
      app.ToolgeLoading(false);
      this.error = true;
      console.log(error)
    }

    
  }

  private mounted() {
    // TODO: stuff to do when this component loads.
    app.ToolgeLoading(false);
  }
}

export {
  Login as default,
  Login,
};
