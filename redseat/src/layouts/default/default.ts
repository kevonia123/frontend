import { Component, Vue } from 'vue-property-decorator';
import { Dictionary } from 'vue-router/types/router';
import env from '@/config/env';
import {  user } from '@/store'
@Component({
  components: {},
  name: 'default',
})
class Default extends Vue {
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------
   private isOpen=false;
   private readonly logo=env.brand.logo;
   private name=""
  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------

  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------
  
  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------

  public close(){
   this.isOpen= false;
  }
  public open(){
   this.isOpen= true;
  }

  public async navigate(path: string, params?: Dictionary<string>) {
    await this.$router.push({ path, params });
  }

  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Lifecycle Hooks
  // --------------------------------------------------------------------------

  private beforeMount() {
    // TODO: stuff to do when this component loads.
    this.name=user.User.name;
  }
  private mounted() {
    // TODO: stuff to do when this component loads.

  }
}

export {
  Default as default,
  Default,
};
