import { AxiosResponse } from 'axios';
import { BaseService } from './base';
import { UserPackage } from '@/entities/user-package/user-package';
import { User } from '@/entities/user/user';

class UserService extends BaseService {
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------
  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------

  public async getByid(id:number): Promise<User> {

    return  await (await this.api.get(`user/${id}`)).data
  }

  public async existsByEmail(email:String): Promise<boolean> {

    return  await (await this.api.get(`user/checkuser/${email}/`)).data
  }
  



  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

}

// ----------------------------------------------------------------------------
// Module Exports
// ----------------------------------------------------------------------------

const service = new UserService();

export {
  service as default,
  service as UserServices,
};
