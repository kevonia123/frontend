import { AxiosResponse } from 'axios';
import { BaseService } from './base';
import { UserRegistration } from '@/entities/user-registration/user-registration';

class RegisterService extends BaseService {
  // --------------------------------------------------------------------------
  // [Private] Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Constructor
  // --------------------------------------------------------------------------
  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // [Public] Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Public] Methods
  // --------------------------------------------------------------------------
  public async  Signup(userdata:UserRegistration): Promise<string> {
    // define custom request options [NB: default config found in @/services/base]
    return  await (await this.api.post(`api/auth/signup`,userdata)).data
  }

  // --------------------------------------------------------------------------
  // [Private] Event Handlers
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // [Private] Methods
  // --------------------------------------------------------------------------

}

// ----------------------------------------------------------------------------
// Module Exports
// ----------------------------------------------------------------------------

const service  = new RegisterService();

export {
  service as default,
  service as Register,
};
