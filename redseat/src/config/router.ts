import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      meta: {

        layout: 'default',
      },
      component: () =>
        import(
          '@/pages/dashboard'),
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        layout: 'minimal',
      },
      component: () =>
        import(
          '@/pages/login'),
    },

    {
      path: '/register',
      name: 'register',
      meta: {
        layout: 'minimal',
      },
      component: () =>
        import(
          '@/pages/register'),
    },

    {
      path: '/package/:id',
      name: 'package',
      meta: {
        layout: 'default',
      },
      component: () => import("@/pages/package")

    },
    {
      path: "*",
      meta: {
        layout: 'minimal',
      },
      component: () => import("@/pages/notfound")

    },

  ],
});
