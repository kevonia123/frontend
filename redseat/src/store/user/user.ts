import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { User } from '@/entities/user/user';
import { LoginRespond } from '@/entities/login-respond/login-respond';
import { Login } from '@/services/auth-login';
import { UserServices } from '@/services/user-services';
import { Loginrequest } from '@/entities/loginrequest/loginrequest';
const MODULE_NAME = 'userstore';
@Module({ namespaced: true })
class UserStore {

    public static readonly getModule = () => ({
        [MODULE_NAME]: UserStore as any as VuexModule
    });
    private _user!: User;

    private _loginresponse!: LoginRespond;

    // ------------------------------------------------------------------------
    // Getters retrieve properties from the Store.
    // ------------------------------------------------------------------------

    public get User() {
        return this._user;
    }

    public get Loginresponse() {
        return this._loginresponse;
    }

    // ------------------------------------------------------------------------
    // Actions are publicly accessbile wrappers to perform mutations
    // on the Store. These actions will internally call the appropriate
    // mutations to update the Store.
    //
    // Note: The returned value will be passed to the mutation handler
    // specified as the decorator's "commit" attribute.
    // ------------------------------------------------------------------------

    @Action({ commit: 'setUser' })
    public async getUser(id:number) {
        const result = await UserServices.getByid(id);
        return result;
    }

    @Action({ commit: 'setLogin' })
    public async Login(userdata:Loginrequest) {
        const result = await Login.Login(userdata);
        return result;
    }


    // ------------------------------------------------------------------------
    // Mutations update the properties in the Store.
    // They are internal
    // ------------------------------------------------------------------------

    @Mutation
    private setUser(value: User) {
        this._user = value;
    }

    @Mutation
    private setLogin(value: LoginRespond) {
        this._loginresponse = value;
    }
}

export {
    UserStore as default,
    UserStore,
};
