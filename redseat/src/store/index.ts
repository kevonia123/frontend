import Vue from 'vue';
import Vuex, { ModuleTree } from 'vuex';
import { UserPackageStore } from './user-package/user-package';
import { UserStore } from './user/user'
import { PackageInvoiceStore } from './package-invoice/package-invoice'
import { AppStore } from './app'
import { MenuitemStore } from './menu-item/menu-item'
import { InvoiceRequestStore } from './invoice-request-store/invoice-request-store'
import { createStoreWrapper } from '@/modules/core/store-wrapper';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);


// Module Constants
// ----------------------------------------------------------------------------
const store = new Vuex.Store<any>({
  modules: {
    ...UserPackageStore.getModule(),
    ...AppStore.getModule(),
    ...UserStore.getModule(),
    ...MenuitemStore.getModule(),
    ...PackageInvoiceStore.getModule(),
    ...InvoiceRequestStore.getModule()
    /* Import other custom modules here. */
  },
  plugins: [createPersistedState({ storage: window.sessionStorage })],
});

const getName = (T: { getModule(): {} }) => Object.keys(T.getModule())[0]
const userpackage = createStoreWrapper<UserPackageStore>(store, getName(UserPackageStore));
const user = createStoreWrapper<UserStore>(store, getName(UserStore));
const menuItems =  createStoreWrapper<MenuitemStore>(store, getName(MenuitemStore));
const app =  createStoreWrapper<AppStore>(store, getName(AppStore));
const packageInvoice =  createStoreWrapper<PackageInvoiceStore>(store, getName(PackageInvoiceStore));
const invoicerequest=  createStoreWrapper<InvoiceRequestStore>(store, getName(InvoiceRequestStore));
// ----------------------------------------------------------------------------
// Exports
// ----------------------------------------------------------------------------
export {
  store as default,
  userpackage,
  user,
  menuItems,
  app,
  packageInvoice,
  invoicerequest
};
