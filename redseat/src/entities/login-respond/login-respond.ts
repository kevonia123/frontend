import { Role } from '@/enums/role';

export interface LoginRespond {
    id: number;
    email: string;
    accessToken: string;
    tokenType:string;
    roles:Role[]
}
