import { Country } from '../country/country';

export interface Address {
    id: number;
    addressline1: string;
    addressline2: string;
    zipcode:string;
    country:Country[];
}
