export interface InvoiceRequest {
    name: string;
    base64: string;
    type: string;
    username: string;
    packageid: number;
}
