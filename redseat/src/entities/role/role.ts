import { Role } from '@/enums/role'
import { Menu } from '../menu/menu';

export interface Roles {
    id: number |undefined;
    name:string |undefined;
    menuId?:Menu |undefined;
}

