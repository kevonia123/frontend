export interface Country {
    id: number;
    countryname: string;
    countrycode: string;
    code:string
}
