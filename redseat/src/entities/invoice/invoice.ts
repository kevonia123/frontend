export interface Invoice {
    id: number;
    name:String;
    data:String;
    type:String;
    created_at: Date;
    update_at: Date;
    update_by: String;
}
