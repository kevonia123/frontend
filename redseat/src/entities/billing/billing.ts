import { Fees } from '../fees/fees';

export interface Billing {
    id: number;
    description: number;
    status: string;
    fee:Fees[]
}
