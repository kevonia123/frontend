import { Roles } from '../role/role';

export interface UserRegistration {
    name: string;
    email:string
    password: string;
    passwordConfirmation: string;
    phone: string;
    addressLine1: string;
    addressLine2: string;
    zipCode: string;
    role: String[];
}
