export interface Fees {
    id: number;
    name: string;
    value: number;
}

