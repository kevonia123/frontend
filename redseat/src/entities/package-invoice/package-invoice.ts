import { Package } from '../package/package';
import { Invoice } from '../invoice/invoice';
import { Billing } from '../billing/billing';

export interface PackageInvoice {
    packageItem: Package;
    invoices: Invoice[];
    billing:Billing;
}
