import { Package } from '../package/package';

export interface UserPackage {
    id: number;
    name: string;
    packlist: Package[];
}
