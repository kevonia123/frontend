import { Roles } from '../role/role';
import { Phone } from '../phone/phone';
import { Address } from '../address/address';

export interface User {
    [x: string]: any;
    id: number;
    name: string;
    email: string;
    roles:Roles[];
    phone:Phone[];
    address:Address[];
}
