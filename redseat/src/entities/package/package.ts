import { PackageLocation } from '@/enums/packagelocation';

import { PackagesStatus } from '@/enums/packagesstatus';

export interface Package {
    id: number;
    trackingNumber: string;
    description: string;
    weight:number
	value:number;
    Status:PackagesStatus;
    location:PackageLocation;
}
